## Introduction

This is one more impementation of Sudoku written on Lazarus/Free Pascal. This is experimental 
pet-project.

<img src="https://gitlab.com/oleglnk/pets-assets/-/raw/main/distribution/sudoku/screenshots/game.png" alt="game" width="400"/>
<img src="https://gitlab.com/oleglnk/pets-assets/-/raw/main/distribution/sudoku/screenshots/game_in_progress.png" alt="game in progress" width="400"/>
<img src="https://gitlab.com/oleglnk/pets-assets/-/raw/main/distribution/sudoku/screenshots/game_failure.png" alt="game failure" width="400"/>
<img src="https://gitlab.com/oleglnk/pets-assets/-/raw/main/distribution/sudoku/screenshots/game_success.png" alt="game success" width="400"/>

## Structure

There are 7 modules:
- uFormMain contains main form (window).
- uFormLoadSave contains form to load/save games.
- uFormOptions contains form to work with game settings.
- uFormStats contains form to show game statistics.
- uFormAbout contains form to show about information.
- uSettings contains game settings and themes implementation.
- uGameSudoku contains Sudoku implementaion.

## Build

The project was created in Lazarus, so you need: 
1. Download Lazarus from [official site](https://www.lazarus-ide.org), 
2. Install it.
3. Build the project.

## Possible improvements

1. Use circular context (pop-up) menu instead of default context menu [e.g.](https://wiki.lazarus.freepascal.org/PospoLite)
2. Replace X and Y panels in status bar to theme selector panel.
3. Replace game over messages to something more cheerful.
4. Change the bsBDiagonal line width and gaps for solved/correct/incorrect cells.
5. Add sudoku solver.
6. Add step-by-step solve animation.
7. A lot of different small optimizations and changes.

## Issues

I want to believe that I removed all of them, but if you find any issues, please tell me.
