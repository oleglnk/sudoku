program sudoku_prj;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  { you can add units after this }
  uControls,
  uSettings,
  uGameSudoku,
  uFormMain,
  uFormOptions,
  uFormStats,
  uFormAbout,
  uFormLoadSave,
  uUtils,
  uDataNConst;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormOptions, FormOptions);
  Application.CreateForm(TFormStats, FormStats);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormLoadSave, FormLoadSave);
  Application.Run;
end.
