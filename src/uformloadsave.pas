unit uFormLoadSave;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, FileUtil,
  uGameSudoku,
  uSettings;

type
  KFormKind = (kfkLoad, kfkSave);

  { TFormLoadSave }

  TFormLoadSave = class(TForm)
    BtnDelete : TButton;
    BtnDo : TButton;
    EditSaveGameName : TEdit;
    GroupGameList : TGroupBox;
    GroupPreview : TGroupBox;
    GroupDescription : TGroupBox;
    GroupSaveGame : TGroupBox;
    LabelDescription : TLabel;
    GamesList : TListBox;
    BoxPreview : TPaintBox;
    procedure BtnDeleteClick(Sender : TObject);
    procedure BtnDoClick(Sender : TObject);
    procedure FormCreate(Sender : TObject);
    procedure FormDestroy(Sender : TObject);
    procedure FormShow(Sender : TObject);
    procedure GamesListSelectionChange(Sender : TObject; User : boolean);
    procedure BoxPreviewPaint(Sender : TObject);
  private
    procedure ResetControls;
    procedure ClearSavedGameList;
    procedure LoadSavedGameList;
  public
    mKind          : KFormKind;
    SavedGamesList : GGameStateList;
    IsGameLoad     : Boolean;
  end;

var
  FormLoadSave : TFormLoadSave;

implementation

uses
  uUtils,
  uDataNConst;

{$R *.lfm}

{ TFormLoadSave }

procedure TFormLoadSave.ResetControls;
begin
  GamesList.ItemIndex   := -1;
  BtnDelete.Enabled        := False;
  EditSaveGameName.Text    := '';
  LabelDescription.Caption := 'Select game to see details';
  BoxPreview.Invalidate;
end;

procedure TFormLoadSave.ClearSavedGameList;
var
  i : Integer;
begin
  for i := 0 to SavedGamesList.Count - 1 do
    SavedGamesList.Data[i].Free;
  SavedGamesList.Clear;
  GamesList.Clear;
end;

procedure TFormLoadSave.LoadSavedGameList;
var
  game      : GGameState;
  i         : Integer;
  filenames : TStringList;
  filename  : String;
begin
  if DirectoryExists(DefaultSavesDir) then
  begin
    filenames := FindAllFiles(DefaultSavesDir, '*' + DefaultSavesExt, False);
    for i := 0 to filenames.Count - 1 do
    begin
      game := GGameState.Create;
      filename := filenames[i].Substring(Length(DefaultSavesDir));
      game.Load(filenames[i]);
      game.mField.SetTheme(GameSettings.CurTheme);
      SavedGamesList.Add(filename, game);
      GamesList.Items.Add(SavedGamesList.Keys[i]);
    end;
    filenames.Free;
  end;
end;

procedure TFormLoadSave.FormCreate(Sender : TObject);
begin
  SavedGamesList := GGameStateList.Create;
end;

procedure TFormLoadSave.FormDestroy(Sender : TObject);
begin
  ClearSavedGameList;
  SavedGamesList.Destroy;
end;

procedure TFormLoadSave.FormShow(Sender : TObject);
begin
  ClearSavedGameList;
  ResetControls;
  case mKind of
   kfkLoad:
     begin
       Caption               := 'Game loading';
       BtnDo.Caption         := 'Load';
       GroupSaveGame.Visible := False;
     end;
   kfkSave:
     begin
       Caption               := 'Game saving';
       BtnDo.Caption         := 'Save';
       GroupSaveGame.Visible := True;
     end;
  end;
  LoadSavedGameList;
  IsGameLoad := False;
end;

procedure TFormLoadSave.GamesListSelectionChange(Sender : TObject; User : boolean);
begin
  if GamesList.ItemIndex <> -1 then
  begin
    BtnDelete.Enabled        := True;
    EditSaveGameName.Text    := GamesList.Items[GamesList.ItemIndex];
    with SavedGamesList.KeyData[EditSaveGameName.Text] do
    begin
      LabelDescription.Caption := 'Difficulty:  ' + KDifficultyTitles[mDifficulty] + LineEnding +
                                 'Hints left:  '   + IntToStr(HintCount) + LineEnding +
                                 'Time passed: '   + SecondsToStr(CurTime);
      mField.Resize(BoxPreview.Width, BoxPreview.Height);
      mField.Draw(BoxPreview.Canvas);
    end;
  end;
end;

procedure TFormLoadSave.BoxPreviewPaint(Sender : TObject);
begin
  with GamesList do
    if ItemIndex <> -1 then
      SavedGamesList.KeyData[Items[ItemIndex]].mField.Draw(BoxPreview.Canvas);
end;

procedure TFormLoadSave.BtnDeleteClick(Sender : TObject);
var
  filename : String;
begin
  filename := GamesList.Items[GamesList.ItemIndex];
  DeleteFile(DefaultSavesDir + filename);
  SavedGamesList.KeyData[filename].Destroy;
  SavedGamesList.Remove(filename);
  GamesList.Items.Delete(GamesList.ItemIndex);
  ResetControls;
end;

procedure TFormLoadSave.BtnDoClick(Sender : TObject);
var
  foundInList : Boolean;
begin
  case mKind of
  kfkLoad:
     if GamesList.ItemIndex = -1 then
       MessageDlg('Game loading', 'No game selected. Please retry', mtError, [mbOK], 0)
     else
     begin
       GameSudoku.Copy(SavedGamesList.KeyData[EditSaveGameName.Text]);
       IsGameLoad := True;
       Close;
     end;
  kfkSave:
    if Trim(EditSaveGameName.Text) = '' then
      MessageDlg('Game saving', 'Name is empty. Please retry', mtError, [mbOK], 0)
    else
    begin
      foundInList := (GamesList.Items.IndexOf(EditSaveGameName.Text) <> -1);
      if (not foundInList) or
         (MessageDlg('Game saving',
                     'Game with such name already exist. Do you want to rewrite it?',
                     mtConfirmation,
                     mbYesNo,
                     0) = mrYes)
      then
      begin
        GameSudoku.Save(DefaultSavesDir + EditSaveGameName.Text + DefaultSavesExt);
        MessageDlg('Game saving', 'Game successfully saved!', mtInformation, [mbOK], 0);
        Close;
      end;
    end;
  end;
end;

end.
