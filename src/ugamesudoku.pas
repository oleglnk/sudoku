unit uGameSudoku;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Graphics, uSettings, fgl;

type

KDifficulty = (kdVeryEasy, kdEasy, kdAverage, kdHard, kdVeryHard);

//
//  KDiffStat
//
KDiffStat = record
  played     : Integer;
  hints_used : Integer;
  best_time  : Integer;
end;

//
//  KStatistics
//
KStatistics = class
const
  dFilename = 'stats.ini';
protected
  mFilename : String;
public
  mDifficulty : array[KDifficulty] of KDiffStat;

  constructor Create;
  destructor  Destroy; override;

  procedure   Load;
  procedure   Save;
  procedure   Clear;
end;

//
//  KCell
//
KCell = record
  user         : Integer;
  value        : Integer;
  is_default   : Boolean;
  boundry      : TRect; // used for drawing
end;

//
//  KField
//
KField = class
protected
  mRowCount       : Integer;
  mColCount       : Integer;
  mCells          : array [1..number_count, 1..number_count] of KCell;
  mArea           : array [0..number_count - 1] of TRect;
  mBGLight        : TBrush;
  mBGDark         : TBrush;
  mCell           : TBrush;
  mAxis           : TPen;
  mSubAxis        : TPen;
  mTheme          : KTheme;
  mWidth          : Integer;
  mHeight         : Integer;
  mTSValues       : TTextStyle;
  mTSNotes        : TTextStyle;
  mCellWidth      : Integer;
  mCellHeight     : Integer;
  mCellEmptyCount : Integer;
  mShowSolved     : Boolean;
  mShowCorrect    : Boolean;
  mSolvedRow      : Integer;
  mSolvedCol      : Integer;

  procedure   Transpose;
  procedure   SwapRowsBig  (const          row1, row2 : Integer);
  procedure   SwapColsBig  (const          col1, col2 : Integer);
  procedure   SwapRowsSmall(const big_row, row1, row2 : Integer);
  procedure   SwapColsSmall(const big_col, col1, col2 : Integer);

  procedure   UpdateBoundries;
  procedure   ClearCells;
  procedure   SetWidth    (const aValue             : Integer);
  procedure   SetHeight   (const aValue             : Integer);
  function    GetValue    (const aRow, aCol         : Integer) : Integer;
  procedure   SetValue    (const aRow, aCol, aValue : Integer);
  function    IsDefValue  (const aRow, aCol         : Integer) : Boolean;
  procedure   SetDefValue (const aRow, aCol         : Integer; const aValue : Boolean);

public
  constructor Create;
  destructor  Destroy; override;

  procedure   Copy       (const aSource         : KField);
  procedure   Draw       (const aCanvas         : TCanvas);
  procedure   SetTheme   (const aTheme          : KTheme);
  procedure   Resize     (const aWidth, aHeight : Integer);
  procedure   RowColByPos(const aX, aY : Integer; var aRow, aCol : Integer);

  property    Values   [i, j : Integer] : Integer read GetValue   write SetValue;
  property    Defaults [i, j : Integer] : Boolean read IsDefValue write SetDefValue;
  property    Width                     : Integer read mWidth     write SetWidth;
  property    Height                    : Integer read mHeight    write SetHeight;
  property    EmptyCount                : Integer read mCellEmptyCount;
end;

//
//  GGameState
//
GGameState = class
const
  dGameSectionName = 'Game';
var
  mField      : KField;
  mDifficulty : KDifficulty;
private
  mHintSolveOneCount : Integer;
  mHintCheckAllCount : Integer;
  mTimePassed        : Integer;

  function GetHintCount : Integer;
public
  constructor Create;
  destructor  Destroy; override;

  procedure   Copy    (const aSource     : GGameState);
  procedure   Generate(const aDifficulty : KDifficulty);
  function    Load    (const aFilename   : string) : Boolean;
  procedure   Save    (const aFilename   : string);
  procedure   ClearAll;
  procedure   CheckAll;
  function    IsOver : Boolean;

  procedure   SolveOne;
  procedure   Solve;

  property    HintSolveOne : Integer read mHintSolveOneCount;
  property    HintCheckAll : Integer read mHintCheckAllCount;
  property    HintCount    : Integer read GetHintCount;
  property    CurTime      : Integer read mTimePassed write mTimePassed;
end;

GGameStateList = specialize TFPGMap<String, GGameState>;


var
  GameStats  : KStatistics;
  GameSudoku : GGameState;

implementation

uses 
  inifiles,
  uDataNConst;


{-------------------------------------------------------------------------------

                                  KStatistics

-------------------------------------------------------------------------------}

constructor KStatistics.Create;
begin
  mFilename := dFilename;
  Clear;
end;

destructor KStatistics.Destroy;
begin
  inherited;
end;

procedure KStatistics.Clear;
var
  i : KDifficulty;
begin
  for i := kdVeryEasy to kdVeryHard do
  begin
    mDifficulty[i].played     := 0;
    mDifficulty[i].hints_used := 0;
    mDifficulty[i].best_time  := 0;
  end;
end;

procedure   KStatistics.Load;
var
  i       : KDifficulty;
  iniFile : TIniFile;
begin
  iniFile := TIniFile.Create(mFilename);
  for i := kdVeryEasy to kdVeryHard do
  begin
    mDifficulty[i].played     := iniFile.ReadInteger(KDifficultyTitles[i], 'played',    0);
    mDifficulty[i].hints_used := iniFile.ReadInteger(KDifficultyTitles[i], 'hints',     0);
    mDifficulty[i].best_time  := iniFile.ReadInteger(KDifficultyTitles[i], 'best time', 0);
  end;
  iniFile.Free;
end;

procedure   KStatistics.Save;
var
  i       : KDifficulty;
  iniFile : TIniFile;
begin
  iniFile := TIniFile.Create(mFilename);
  for i := kdVeryEasy to kdVeryHard do
  begin
    iniFile.WriteInteger(KDifficultyTitles[i], 'played',    mDifficulty[i].played);
    iniFile.WriteInteger(KDifficultyTitles[i], 'hints',     mDifficulty[i].hints_used);
    iniFile.WriteInteger(KDifficultyTitles[i], 'best time', mDifficulty[i].best_time);
  end;
  iniFile.Free;
end;


{-------------------------------------------------------------------------------

                                    KField

-------------------------------------------------------------------------------}

constructor KField.Create;
begin
  inherited Create;
  mRowCount := number_count;
  mColCount := number_count;

  // background
  mBGLight        := TBrush.Create;
  mBGLight.Style  := bsSolid;

  mBGDark       := TBrush.Create;
  mBGDark.Style := bsSolid;

  // cell Type
  mCell       := TBrush.Create;
  mCell.Style := bsBDiagonal;
  
  // Axis
  mAxis       := TPen.Create;
  mAxis.Style := psSolid;
  mAxis.Width := 4;
  
  mSubAxis        := TPen.Create;
  mSubAxis.Style  := psDash;
  mSubAxis.Width  := 2;

  // Value Style
  mTSValues.Alignment  := taCenter;
  mTSValues.Layout     := tlCenter;
  mTSValues.SystemFont := False;

  // Notes Style
  mTSNotes.Alignment   := taLeftJustify;
  mTSNotes.Layout      := tlTop;
  mTSNotes.SystemFont  := False;

  Width  := 0;
  Height := 0;
  SetTheme(DefaultTheme);
  ClearCells;
end;

destructor KField.Destroy;
begin
  mBGLight.Free;
  mBGDark.Free;
  mCell.Free;
  mAxis.Free;
  mSubAxis.Free;
  inherited Destroy;
end;

procedure KField.Copy(
    const aSource : KField);
var
  i, j : Integer;
begin
  mCellEmptyCount := aSource.mCellEmptyCount;
  for i := 1 to mRowCount do
    for j := 1 to mColCount do
    begin
      mCells[i, j].user         := aSource.mCells[i, j].user;
      mCells[i, j].value        := aSource.mCells[i, j].value;
      mCells[i, j].is_default   := aSource.mCells[i, j].is_default;
    end;
  mShowSolved  := False;
  mShowCorrect := False;
end;

procedure KField.Draw(
    const aCanvas : TCanvas);

  function GetAreaBrush(
      const aRow, aCol : Integer) : TBrush;
  var
    i, j : Integer;
  begin
    j := (number_count div 3);
    i := ((aRow - 1) div j) * j + ((aCol - 1) div j);
    Result := mBGLight;
    if (i mod 2 <> 0) then
     Result := mBGDark;
  end;

const
  nums : array[1..number_count] of String = (
  '1', '2', '3', '4', '5', '6', '7', '8', '9');
var
  i, j : integer;
begin

//  Draw Background
  for i := 0 to number_count - 1 do
  begin
    if (i mod 2 = 0) then
       aCanvas.Brush := mBGLight
     else
       aCanvas.Brush := mBGDark;
    aCanvas.FillRect(mArea[i]);
  end;

//  Highlight solved
  if mShowSolved then
  begin
    aCanvas.Brush := GetAreaBrush(mSolvedRow, mSolvedCol);
    aCanvas.FillRect(mCells[mSolvedRow, mSolvedCol].boundry);

    mCell.Color  := mTheme.solved;
    aCanvas.Brush := mCell;
    aCanvas.FillRect(mCells[mSolvedRow, mSolvedCol].boundry);
  end;

//  Highlight Checked
  if mShowCorrect then
  begin
    for i := 1 to mRowCount do
      for j := 1 to mColCount do
        if (mCells[i, j].user <> 0) and
           (not mCells[i, j].is_default) then
        begin
          aCanvas.Brush := GetAreaBrush(i, j);
          aCanvas.FillRect(mCells[i, j].boundry);
          if (mCells[i, j].user = mCells[i, j].value) then
            mCell.Color := mTheme.correct
          else
            mCell.Color := mTheme.incorrect;
          aCanvas.Brush  := mCell;
          aCanvas.FillRect(mCells[i, j].boundry);
        end;
  end;

//  Draw axis
  aCanvas.Pen   := mAxis;
  //  vertical
  aCanvas.Line(mCellWidth * 3, 0, mCellWidth * 3, mHeight);
  aCanvas.Line(mCellWidth * 6, 0, mCellWidth * 6, mHeight);
  //  horizontal
  aCanvas.Line(0, mCellHeight * 3, mWidth, mCellHeight * 3);
  aCanvas.Line(0, mCellHeight * 6, mWidth, mCellHeight * 6);

//  Draw sub axis
  aCanvas.Pen := mSubAxis;

  i := 0;
  while (i < 3) do
  begin
    j := i * 3;
    //  vertical
    aCanvas.Line(mCellWidth * (j + 1), 0, mCellWidth * (j + 1), mHeight);
    aCanvas.Line(mCellWidth * (j + 2), 0, mCellWidth * (j + 2), mHeight);

    //  horizontal
    aCanvas.Line(0, mCellHeight * (j + 1), mWidth, mCellHeight * (j + 1));
    aCanvas.Line(0, mCellHeight * (j + 2), mWidth, mCellHeight * (j + 2));
    i := i + 1;
  end;

//  Draw Values
  aCanvas.Font.Name   := 'Arial';
  aCanvas.Font.Height := mCellHeight;
  aCanvas.TextStyle   := mTSValues;
  for i := 1 to mRowCount do
    for j := 1 to mColCount do
      if (mCells[i, j].user <> 0) then
      begin
        aCanvas.Font.Color := mTheme.numbers[mCells[i, j].user];
        if mCells[i, j].is_default then
          aCanvas.Font.Style := []
        else
          aCanvas.Font.Style := [fsItalic];
        aCanvas.TextRect(mCells[i, j].boundry, 0, 0, nums[mCells[i, j].user]);
      end;
end;

procedure KField.SetTheme(
    const aTheme : KTheme);
begin
  mTheme         := aTheme;
  mBGLight.Color := mTheme.bg_light;
  mBGDark.Color  := mTheme.bg_dark;
  mAxis.Color    := mTheme.grid;
  mSubAxis.Color := mTheme.grid;
end;

procedure KField.Resize(
    const aWidth, aHeight : Integer);
begin
  mWidth      := aWidth;
  mHeight     := aHeight;
  mCellWidth  := mWidth  div mColCount;
  mCellHeight := mHeight div mRowCount;
  UpdateBoundries;
end;

procedure KField.Transpose;
var
  row, col, temp : Integer;
begin
  for row := 1 to mRowCount do
    for col := row to mColCount do
    begin
      temp                  := mCells[row, col].value;
      mCells[row, col].value := mCells[col, row].value;
      mCells[col, row].value := temp;
    end;
end;

procedure KField.SwapRowsBig(
    const row1, row2 : Integer);
var
  temp, col : Integer;
begin
  for col := 1 to mColCount do
  begin
    temp                            := mCells[row1 * 3, col].value;
    mCells[row1 * 3,     col].value := mCells[row2 * 3, col].value;
    mCells[row2 * 3,     col].value := temp;

    temp                            := mCells[row1 * 3 - 1, col].value;
    mCells[row1 * 3 - 1, col].value := mCells[row2 * 3 - 1, col].value;
    mCells[row2 * 3 - 1, col].value := temp;

    temp                            := mCells[row1 * 3 - 2, col].value;
    mCells[row1 * 3 - 2, col].value := mCells[row2 * 3 - 2, col].value;
    mCells[row2 * 3 - 2, col].value := temp;
  end;
end;

procedure KField.SwapColsBig(
    const col1, col2 : Integer);
var
  temp, row : Integer;
begin
  for row := 1 to mRowCount do
  begin
    temp                            := mCells[row, col1 * 3].value;
    mCells[row, col1 * 3].value     := mCells[row, col2 * 3].value;
    mCells[row, col2 * 3].value     := temp;

    temp                            := mCells[row, col1 * 3 - 1].value;
    mCells[row, col1 * 3 - 1].value := mCells[row, col2 * 3 - 1].value;
    mCells[row, col2 * 3 - 1].value := temp;

    temp                            := mCells[row, col1 * 3 - 2].value;
    mCells[row, col1 * 3 - 2].value := mCells[row, col2 * 3 - 2].value;
    mCells[row, col2 * 3 - 2].value := temp;
  end;
end;

procedure KField.SwapRowsSmall(
    const big_row, row1, row2 : Integer);
var
  temp, col : Integer;
begin
  for col := 1 to mColCount do
  begin
    temp                                  := mCells[big_row * 3 - row1, col].value;
    mCells[big_row * 3 - row1, col].value := mCells[big_row * 3 - row2, col].value;
    mCells[big_row * 3 - row2, col].value := temp;
  end;
end;

procedure KField.SwapColsSmall(
    const big_col, col1, col2 : Integer);
var
  temp, row : Integer;
begin
  for row := 1 to mRowCount do
  begin
    temp                                  := mCells[row, big_col * 3 - col1].value;
    mCells[row, big_col * 3 - col1].value := mCells[row, big_col * 3 - col2].value;
    mCells[row, big_col * 3 - col2].value := temp;
  end;
end;

procedure KField.UpdateBoundries;
var
  i, j : Integer;
begin
  for i := 1 to mRowCount do
    for j := 1 to mColCount do
      with mCells[i, j].boundry do
      begin
        Left   := mCellWidth  * (j - 1);
        Top    := mCellHeight * (i - 1);
        Right  := Left + mCellWidth;
        Bottom := Top  + mCellHeight;
      end;
  for i := 0 to number_count - 1 do
    with mArea[i] do
    begin
      Left   := (mWidth  div 3) * (i mod (number_count div 3));
      Top    := (mHeight div 3) * (i div (number_count div 3));
      Right  := Left + (mWidth  div 3) + 2;
      Bottom := Top  + (mHeight div 3) + 2;
    end;
end;

procedure KField.ClearCells;
var
  i, j : Integer;
begin
  mCellEmptyCount := mRowCount * mColCount;
  for i := 1 to mRowCount do
    for j := 1 to mColCount do
    begin
      mCells[i, j].user       := 0;
      mCells[i, j].value      := 0;
      mCells[i, j].is_default := False;
    end;
  mShowSolved  := False;
  mShowCorrect := False;
end;

procedure KField.SetWidth(
    const aValue  : Integer);
begin
  mWidth      := aValue;
  mCellWidth  := mWidth div mColCount;
  UpdateBoundries;
end;

procedure KField.SetHeight(
    const aValue : Integer);
begin
  mHeight    := aValue;
  mCellHeight := mHeight div mRowCount;
  UpdateBoundries;
end;

function KField.GetValue(
    const aRow, aCol : Integer) : Integer;
begin
  Result := mCells[aRow, aCol].user;
end;

procedure KField.SetValue(
    const aRow, aCol, aValue : Integer);
begin
  with mCells[aRow, aCol] do
  begin
    if (user = 0) and (aValue <> 0) then
      mCellEmptyCount := mCellEmptyCount - 1;
    if (user <> 0) and (aValue = 0) then
      mCellEmptyCount := mCellEmptyCount + 1;

    user         := aValue;
    mShowCorrect := False;
    mShowSolved  := False;
  end;
end;

function KField.IsDefValue(
    const aRow, aCol      : Integer) : Boolean;
begin
  Result := mCells[aRow, aCol].is_default;
end;

procedure KField.SetDefValue(
    const aRow, aCol : Integer; const aValue : Boolean);
begin
  if mCells[aRow, aCol].user = 0 then
    mCellEmptyCount := mCellEmptyCount - 1;

  mCells[aRow, aCol].user       := mCells[aRow, aCol].value;
  mCells[aRow, aCol].is_default := aValue;
end;

procedure KField.RowColByPos(
    const aX, aY     : Integer;
    var   aRow, aCol : Integer);
begin
  aRow := (aY div mCellHeight) + 1;
  aCol := (aX div mCellWidth) + 1;
end;



{-------------------------------------------------------------------------------

                                    GGameState

-------------------------------------------------------------------------------}

constructor GGameState.Create;
begin
  inherited Create;

  mField              := KField.Create;
  mHintSolveOneCount  := 0;
  mHintCheckAllCount  := 0;
  mTimePassed         := 0;
end;

destructor GGameState.Destroy;
begin
  mField.Free;
  inherited Destroy;
end;

procedure GGameState.Copy(
    const aSource : GGameState);
begin
  mHintSolveOneCount  := aSource.mHintSolveOneCount;
  mHintCheckAllCount  := aSource.mHintCheckAllCount;
  mTimePassed         := aSource.mTimePassed;
  mDifficulty         := aSource.mDifficulty;
  mField.Copy(aSource.mField);
end;

procedure GGameState.Generate(
    const aDifficulty : KDifficulty);
const
  opened_max_count : array[KDifficulty] of Integer =
    (49, 41, 33, 25, 17);
const basic_table : array[1..number_count, 1..number_count] of Integer =
  ((1, 2, 3, 4, 5, 6, 7, 8, 9),
   (4, 5, 6, 7, 8, 9, 1, 2, 3),
   (7, 8, 9, 1, 2, 3, 4, 5, 6),

   (2, 3, 4, 5, 6, 7, 8, 9, 1),
   (5, 6, 7, 8, 9, 1, 2, 3, 4),
   (8, 9, 1, 2, 3, 4, 5, 6, 7),

   (3, 4, 5, 6, 7, 8, 9, 1, 2),
   (6, 7, 8, 9, 1, 2, 3, 4, 5),
   (9, 1, 2, 3, 4, 5, 6, 7, 8));
var
  row, col, n, i, r1, r2 : Integer;
begin
  mField.ClearCells;
  mDifficulty := aDifficulty;
  mHintSolveOneCount := KDifficultyMaxHints[mDifficulty, 0];
  mHintCheckAllCount := KDifficultyMaxHints[mDifficulty, 1];

  for row := 1 to mField.mRowCount do
    for col := 1 to mField.mColCount do
      mField.mCells[row, col].value := basic_table[row, col];
  n := Random(1000);
  for i := 1 to n do
  begin
    case (Random(10000) mod 5) of
    0: mField.Transpose;
    1:
      begin
        r1 := Random(3) + 1;
        r2 := Random(3) + 1;
        mField.SwapColsBig(r1, r2);
      end;
    2:
      begin
        r1 := Random(3) + 1;
        r2 := Random(3) + 1;
        mField.SwapRowsBig(r1, r2);
      end;
    3:
      begin
        col := Random(3) + 1;
        r1  := Random(3);
        r2  := Random(3);
        mField.SwapColsSmall(col, r1, r2);
      end;
    4:
      begin
        row := Random(3) + 1;
        r1  := Random(3);
        r2  := Random(3);
        mField.SwapRowsSmall(row, r1, r2);
      end;
    end;
  end;

  i := 1;
  while (i <= opened_max_count[mDifficulty]) do
  begin
    row := Random(mField.mRowCount) + 1;
    col := Random(mField.mColCount) + 1;
    if not(mField.mCells[row, col].is_default) then
    begin
      mField.mCells[row, col].user       := mField.mCells[row, col].value;
      mField.mCells[row, col].is_default := True;
      i := i + 1;
    end;
  end;
  mField.mCellEmptyCount := mField.mColCount * mField.mRowCount - opened_max_count[mDifficulty];
  mTimePassed := 0;
end;

function GGameState.Load(
    const aFilename : String) : Boolean;
var
  iniFile       : TIniFile;
  i, j          : Integer;
  section       : string;
  isCheatingBad : Boolean;
  diffName      : string;
begin
  iniFile                  := TMemIniFile.Create(aFilename);
  iniFile.Options          := iniFile.Options + [ifoWriteStringBoolean];
  iniFile.BoolTrueStrings  := ['true'];
  iniFile.BoolFalseStrings := ['false'];

  diffName           := iniFile.ReadString (dGameSectionName, 'difficulty', '');
  mHintSolveOneCount := iniFile.ReadInteger(dGameSectionName, 'solve one',  0);
  mHintCheckAllCount := iniFile.ReadInteger(dGameSectionName, 'check all',  0);
  mTimePassed        := iniFile.ReadInteger(dGameSectionName, 'time',       mTimePassed);

  isCheatingBad := False;
  if (diffName = KDifficultyTitles[kdVeryEasy]) then
    mDifficulty := kdVeryEasy
  else if (diffName = KDifficultyTitles[kdEasy]) then
    mDifficulty := kdEasy
  else if (diffName = KDifficultyTitles[kdAverage]) then
    mDifficulty := kdAverage
  else if (diffName = KDifficultyTitles[kdHard]) then
    mDifficulty := kdHard
  else if (diffName = KDifficultyTitles[kdVeryHard]) then
    mDifficulty := kdVeryHard
  else
    isCheatingBad := True;

  // Cheating is bad
  if (isCheatingBad) or
     (mHintSolveOneCount > KDifficultyMaxHints[mDifficulty, 0]) or
     (mHintSolveOneCount < 0) or
     (mHintCheckAllCount > KDifficultyMaxHints[mDifficulty, 0]) or
     (mHintCheckAllCount < 0) or
     (mTimePassed < 0)
     then
  begin
    Generate(kdVeryHard);
    mHintCheckAllCount  := 0;
    mHintSolveOneCount  := 0;
    mTimePassed         := 300;
    isCheatingBad       := True; // Cheating is very bad
  end
  else
  begin
    for i := 1 to mField.mRowCount do
      for j := 1 to mField.mColCount do
      begin
        section := 'Cell ' + IntToStr(i) + ' ' + IntToStr(j);
        mField.mCells[i, j].user       := iniFile.ReadInteger(section, 'user',  0);
        mField.mCells[i, j].value      := iniFile.ReadInteger(section, 'value', 0);
        mField.mCells[i, j].is_default := iniFile.ReadBool   (section, 'default', False);
      end;
  end;

  for i := 1 to mField.mRowCount do
    for j := 1 to mField.mColCount do
    begin
      if (mField.mCells[i, j].value <= 0) or
         (mField.mCells[i, j].value > 9) or
         (mField.mCells[i, j].user < 0) or
         (mField.mCells[i, j].user > 9) or
         ((mField.mCells[i, j].user <> mField.mCells[i, j].value) and
          (mField.mCells[i, j].is_default))
         then
      begin
        isCheatingBad := True; // Cheating is very bad
        Break;
      end;

      if isCheatingBad then
      begin
        Generate(kdVeryHard);
        Break;
      end;
    end;

  iniFile.Free;
  Result := True;
end;

procedure GGameState.Save(
    const aFilename : String);
var
  iniFile : TMemIniFile;
  i, j    : Integer;
  section : String;
begin
  iniFile                  := TMemIniFile.Create(aFilename);
  iniFile.Options          := iniFile.Options + [ifoWriteStringBoolean];
  iniFile.BoolTrueStrings  := ['true'];
  iniFile.BoolFalseStrings := ['false'];

  iniFile.WriteString (dGameSectionName, 'difficulty', KDifficultyTitles[mDifficulty]);
  iniFile.WriteInteger(dGameSectionName, 'solve one',  mHintSolveOneCount);
  iniFile.WriteInteger(dGameSectionName, 'check all',  mHintCheckAllCount);
  iniFile.WriteInteger(dGameSectionName, 'time',       mTimePassed);

  for i := 1 to mField.mRowCount do
    for j := 1 to mField.mColCount do
    begin
      section := 'Cell ' + IntToStr(i) + ' ' + IntToStr(j);
      iniFile.WriteInteger(section, 'user',    mField.mCells[i, j].user);
      iniFile.WriteInteger(section, 'value',   mField.mCells[i, j].value);
      iniFile.WriteBool   (section, 'default', mField.mCells[i, j].is_default);
    end;

  iniFile.UpdateFile;
  iniFile.Free;
end;

procedure GGameState.ClearAll;
begin
  mField.ClearCells;
  mHintSolveOneCount := 0;
  mHintCheckAllCount := 0;
  mTimePassed        := 0;
end;

procedure GGameState.CheckAll;
begin
  if (mHintCheckAllCount <> 0) then
  begin
    mField.mShowSolved  := False;
    mField.mShowCorrect := True;
    mHintCheckAllCount  := mHintCheckAllCount - 1;
  end;
end;

function GGameState.IsOver : Boolean;
var
  row, col : Integer;
begin
  Result := (mField.EmptyCount = 0);
  if Result then
   for row := 1 to mField.mRowCount do
    for col := 1 to mField.mColCount do
      if (not (mField.Defaults[row, col])) and
         (mField.mCells[row, col].user <> mField.mCells[row, col].value)
         then
      begin
        Result := False;
        Exit;
      end;
end;

procedure GGameState.SolveOne;
begin
  if (mHintSolveOneCount <> 0) then
  begin
    mHintSolveOneCount  := mHintSolveOneCount - 1;
    mField.mShowSolved  := True;
    mField.mShowCorrect := False;
    repeat
      mField.mSolvedRow := Random(mField.mRowCount) + 1;
      mField.mSolvedCol := Random(mField.mColCount) + 1;
    until not mField.Defaults[mField.mSolvedRow, mField.mSolvedCol];
    mField.Defaults[mField.mSolvedRow, mField.mSolvedCol] := True;
  end;
end;

procedure GGameState.Solve;
var
  col, row : integer;
begin
  for row := 1 to mField.mRowCount do
    for col := 1 to mField.mColCount do
      if (not (mField.Defaults[row, col])) and
         (mField.mCells[row, col].user <> mField.mCells[row, col].value)
         then
      begin
        mField.Values[row, col] := mField.mCells[row, col].value;
      end;
end;

function GGameState.GetHintCount : Integer;
begin
  Result := mHintSolveOneCount + mHintCheckAllCount;
end;

end.
