unit uUtils;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

function SecondsToStr(const aSeconds : Integer) : String;

//
//  Working with colors
//

function RandomColor : TColor;
function IsColor(const aString : string) : Boolean;
function TColorToHexStr(const aColor  : TColor) : String;
function HexStrToTColor(const aString : string; aDefault : TColor = clNone) : TColor; 


implementation

{
Converts seconds to formated string like 'mm:ss', where:
  mm - minutes
  ss - seconds
}
function SecondsToStr(const aSeconds : Integer) : String;
begin
  Result := Format('%.2d:%.2d', [aSeconds div 60, aSeconds mod 60]);
end;


{================================
           Color Utils
================================}

//  Generates random TColor
function RandomColor : TColor;
begin
  Result := RGBToColor(Random(256), Random(256), Random(256));
end;

// Check if aString is a TColor in format '#rrggbb'
function IsColor(
    const aString : string) : Boolean;
var
  i, l : Integer;
begin
  l := Length(aString);
  Result := (l = 7) and (aString[1] = '#');
  if Result then
    for i := 2 to l do
      if not(aString[i] in ['0'..'9', 'a'..'f', 'A'..'F']) then
      begin
         Result := False;
         break;
      end;
end;

{
  Converts TColor to String
  Return: string in '#rrggbb' format, empty string if aColor = clNone
}
function TColorToHexStr(
    const aColor : TColor): string;
var
  rgbValue : LongInt;
begin
  Result := '';
  if aColor <> clNone then
  begin
    rgbValue := ColorToRGB(aColor);
    Result := '#' +
      IntToHex(Red  (rgbValue), 2) +
      IntToHex(Green(rgbValue), 2) +
      IntToHex(Blue (rgbValue), 2);
  end;
end;

{
  Converts '#rrggbb' formated string to TColor
  Return: TColor type, aDefault if string is not color
}
function HexStrToTColor(
    const aString : string; 
    aDefault : TColor = clNone) : TColor;
var
  R, G, B : integer;
begin
  Result := aDefault;
  if IsColor(aString) then
  begin
    R := StrToInt('$' + Copy(aString, 2, 2));
    G := StrToInt('$' + Copy(aString, 4, 2));
    B := StrToInt('$' + Copy(aString, 6, 2));
    Result := RGBToColor(R, G, B);
  end;
end;

end.
