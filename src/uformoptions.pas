unit uFormOptions;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  uSettings;

type
  { TFormOptions }

  TFormOptions = class(TForm)
    BevelLineUp      : TBevel;
    BevelLineDown    : TBevel;

    {          Themes Controls          }
    Label_ColorTheme : TLabel;
    CB_Themes        : TComboBox;
    Btn_Add          : TButton;
    Btn_Rename       : TButton;
    Btn_Delete       : TButton;

    {        Cur Theme Controls         }
    PB_Preview          : TPaintBox;
    Btn_Generate        : TButton;
    Label_BGLight       : TLabel;
    Label_BGDark        : TLabel;
    Label_Grid          : TLabel;
    Label_Num1          : TLabel;
    Label_Num2          : TLabel;
    Label_Num3          : TLabel;
    Label_Num4          : TLabel;
    Label_Num5          : TLabel;
    Label_Num6          : TLabel;
    Label_Num7          : TLabel;
    Label_Num8          : TLabel;
    Label_Num9          : TLabel;
    Label_Solved        : TLabel;
    Label_Correct       : TLabel;
    Label_Incorrect     : TLabel;
    ColorDlg            : TColorDialog;
    Btn_Color_BGLight   : TColorButton;
    Btn_Color_BGDark    : TColorButton;
    Btn_Color_Grid      : TColorButton;
    Btn_Color_Num1      : TColorButton;
    Btn_Color_Num2      : TColorButton;
    Btn_Color_Num3      : TColorButton;
    Btn_Color_Num4      : TColorButton;
    Btn_Color_Num5      : TColorButton;
    Btn_Color_Num6      : TColorButton;
    Btn_Color_Num7      : TColorButton;
    Btn_Color_Num8      : TColorButton;
    Btn_Color_Num9      : TColorButton;
    Btn_Color_Solved    : TColorButton;
    Btn_Color_Correct   : TColorButton;
    Btn_Color_Incorrect : TColorButton;

    {          Other Controls           }
    CheckBox_AutoSave        : TCheckBox;
    CheckBox_StartupContinue : TCheckBox;
    Btn_Ok                   : TButton;
    Btn_Cancel               : TButton;

    procedure ResetControls;
    procedure controlsSetTags;

    procedure CheckBox_AutoSaveChange       (Sender : TObject);
    procedure CheckBox_StartupContinueChange(Sender : TObject);
    procedure PB_PreviewPaint               (Sender : TObject);

    { TFormOptions }
    procedure FormCreate       (Sender : TObject);
    procedure FormDestroy      (Sender : TObject);

    {  Theme CRUD  }
    procedure CB_ThemesSelect  (Sender : TObject);

    procedure Btn_AddClick     (Sender : TObject);
    procedure Btn_DeleteClick  (Sender : TObject);
    procedure Btn_RenameClick  (Sender : TObject);

    // That is awful, should use array
    { Color Buttons }
    procedure Btn_Color_Changed(Sender : TObject);
    procedure Btn_GenerateClick(Sender : TObject);

    { Settings save/not }
    procedure Btn_OkClick      (Sender : TObject);
    procedure Btn_CancelClick  (Sender : TObject);

    procedure FormCloseQuery   (Sender : TObject; var CanClose : Boolean);

  private
    isEdited          : Boolean;
    settingsTemp      : KSettings;
    themeTemp         : KTheme;
    mCellWidth        : Integer;
    mCellHeight       : Integer;
    preview_bBGLight  : TBrush;
    preview_bBGDark   : TBrush;
    preview_bCell     : TBrush;
    preview_pen       : TPen;
    preview_rects     : array[1..number_count] of TRect;
    preview_dup_rects : array[1..3] of TRect;
    preview_font      : TFont;
    preview_style     : TTextStyle;
  public
    settingsChanged   : Boolean;
  end;

var
  FormOptions : TFormOptions;

implementation

uses
  uUtils,
  uDataNConst;

const
  TAG_BTNCOLOR_BGLIGHT    = 0;
  TAG_BTNCOLOR_BGDARK     = 1;
  TAG_BTNCOLOR_GRID       = 2;
  TAG_BTNCOLOR_NUM1       = 3;
  TAG_BTNCOLOR_NUM2       = 4;
  TAG_BTNCOLOR_NUM3       = 5;
  TAG_BTNCOLOR_NUM4       = 6;
  TAG_BTNCOLOR_NUM5       = 7;
  TAG_BTNCOLOR_NUM6       = 8;
  TAG_BTNCOLOR_NUM7       = 9;
  TAG_BTNCOLOR_NUM8       = 10;
  TAG_BTNCOLOR_NUM9       = 11;
  TAG_BTNCOLOR_CSOLVED    = 12;
  TAG_BTNCOLOR_CCORRECT   = 13;
  TAG_BTNCOLOR_CINCORRECT = 14;

{$R *.lfm}

procedure TFormOptions.controlsSetTags;
begin
  Btn_Color_BGLight.Tag   := TAG_BTNCOLOR_BGLIGHT;
  Btn_Color_BGDark.Tag    := TAG_BTNCOLOR_BGDARK;
  Btn_Color_Grid.Tag      := TAG_BTNCOLOR_GRID;
  Btn_Color_Num1.Tag      := TAG_BTNCOLOR_NUM1;
  Btn_Color_Num2.Tag      := TAG_BTNCOLOR_NUM2;
  Btn_Color_Num3.Tag      := TAG_BTNCOLOR_NUM3;
  Btn_Color_Num4.Tag      := TAG_BTNCOLOR_NUM4;
  Btn_Color_Num5.Tag      := TAG_BTNCOLOR_NUM5;
  Btn_Color_Num6.Tag      := TAG_BTNCOLOR_NUM6;
  Btn_Color_Num7.Tag      := TAG_BTNCOLOR_NUM7;
  Btn_Color_Num8.Tag      := TAG_BTNCOLOR_NUM8;
  Btn_Color_Num9.Tag      := TAG_BTNCOLOR_NUM9;
  Btn_Color_Solved.Tag    := TAG_BTNCOLOR_CSOLVED;
  Btn_Color_Correct.Tag   := TAG_BTNCOLOR_CCORRECT;
  Btn_Color_Incorrect.Tag := TAG_BTNCOLOR_CINCORRECT;
end;

procedure TFormOptions.ResetControls;
var
  i : Integer;
begin
  CB_Themes.Items.Clear;
  for i := 0 to settingsTemp.mThemes.mDict.Count - 1 do
    CB_Themes.Items.Add(settingsTemp.mThemes.mDict.keys[i]);
  CB_Themes.ItemIndex              := settingsTemp.ItemIndex;
  CheckBox_AutoSave.Checked        := settingsTemp.mAutosave;
  CheckBox_StartupContinue.Checked := settingsTemp.mLoadOnStartup;
  CB_ThemesSelect(Self);
  isEdited                         := False;
end;

procedure TFormOptions.CheckBox_StartupContinueChange(Sender : TObject);
begin
  isEdited := (CheckBox_StartupContinue.Checked <> settingsTemp.mLoadOnStartup);
end;

procedure TFormOptions.CheckBox_AutoSaveChange(Sender : TObject);
begin
  isEdited := (CheckBox_AutoSave.Checked <> settingsTemp.mAutosave);
end;

procedure TFormOptions.PB_PreviewPaint(Sender : TObject);
const
  nums : array[1..number_count] of String = (
  '1', '2', '3', '4', '5', '6', '7', '8', '9');
var
  i : Integer;
begin
  with PB_Preview.Canvas do
  begin
    Pen   := preview_pen;

    // background
    for i := 1 to number_count do
    begin
      if (i mod 2 <> 0) then
        Brush := preview_bBGLight
      else
        Brush := preview_bBGDark;
      FillRect(preview_rects[i]);
    end;

    Line(mCellWidth,     0,               mCellWidth,     Height         );
    Line(mCellWidth * 2, 0,               mCellWidth * 2, Height         );
    Line(0,              mCellHeight,     Width,          mCellHeight    );
    Line(0,              mCellHeight * 2, Width,          mCellHeight * 2);
    Line(0,              mCellHeight * 3, Width,          mCellHeight * 3);

    // cell types

    // Cell => Solved
    Brush               := preview_bBGDark;
    FillRect(0, mCellHeight * 3, mCellWidth, Height);
    preview_bCell.Color := Btn_Color_Solved.ButtonColor;
    Brush               := preview_bCell;
    FillRect(0, mCellHeight * 3, mCellWidth, Height);

    // Cell => Correct
    Brush               := preview_bBGLight;
    FillRect(mCellWidth, mCellHeight * 3, mCellWidth * 2, Height);
    preview_bCell.Color := Btn_Color_Correct.ButtonColor;
    Brush               := preview_bCell;
    FillRect(mCellWidth, mCellHeight * 3, mCellWidth * 2, Height);

    // Cell => InCorrect
    Brush               := preview_bBGDark;
    FillRect(mCellWidth * 2, mCellHeight * 3, Width, Height);

    preview_bCell.Color := Btn_Color_Incorrect.ButtonColor;
    Brush               := preview_bCell;
    FillRect(mCellWidth * 2, mCellHeight * 3, Width, Height);

    // grid
    Line(mCellWidth,     0,               mCellWidth,     Height         );
    Line(mCellWidth * 2, 0,               mCellWidth * 2, Height         );
    Line(0,              mCellHeight,     Width,          mCellHeight    );
    Line(0,              mCellHeight * 2, Width,          mCellHeight * 2);
    Line(0,              mCellHeight * 3, Width,          mCellHeight * 3); 

    // numbers
    Font       := preview_font;
    TextStyle  := preview_style;
    for i := 1 to number_count do
    begin
      Font.Color   := settingsTemp.CurTheme.numbers[i];
      TextRect(preview_rects[i], 0, 0, nums[i]);
    end;
  end;
end;

{ TFormOptions }

procedure TFormOptions.FormCreate(Sender : TObject);
const
  dColCount = 3;
  dRowCount = 4;
var
  x, y : Integer;
begin
  controlsSetTags;

  mCellHeight := PB_Preview.Height div dRowCount;
  mCellWidth  := PB_Preview.Width  div dColCount;

  preview_bBGLight       := TBrush.Create;
  preview_bBGLight.Style := bsSolid;

  preview_bBGDark        := TBrush.Create;
  preview_bBGDark.Style  := bsSolid;

  preview_bCell          := TBrush.Create;
  preview_bCell.Style    := bsBDiagonal;

  preview_pen            := TPen.Create;
  preview_pen.Style      := psDash;
  preview_pen.Width      := 2;



  for x := 0 to 2 do
    for y := 0 to 2 do
      with preview_rects[y * dColCount + x + 1] do
      begin
        Left   := mCellWidth * x;
        Right  := Left + mCellWidth;
        Top    := mCellHeight * y;
        Bottom := Top + mCellHeight;
      end;

  for x := 1 to 3 do
    with preview_dup_rects[x] do
    begin
      Left   := mCellWidth * (x - 1);
      Right  := Left + mCellWidth;
      Top    := mCellHeight * 3;
      Bottom := Top + mCellHeight;
    end;

  preview_font        := TFont.Create;
  preview_font.Height := mCellHeight;
  //preview_font.Style  := [fsItalic];
  preview_font.Name   := 'Arial';

  preview_style.Alignment  := taCenter;
  preview_style.Layout     := tlCenter;
  preview_style.SystemFont := False;

  settingsTemp := KSettings.Create;
  settingsTemp.copy(GameSettings);
  ResetControls;
end;

procedure TFormOptions.FormDestroy(Sender : TObject);
begin
  settingsTemp.Free;
  preview_bCell.Free;
  preview_bBGDark.Free;
  preview_bBGLight.Free;
  preview_pen.Free;
  preview_font.Free;
end;

{  Theme CRUD  }

procedure TFormOptions.CB_ThemesSelect(Sender : TObject);
  procedure EnableControls(status : Boolean);
  begin
    Btn_Delete.Enabled          := status;
    Btn_Rename.Enabled          := status;
    Btn_Generate.Enabled        := status;
    Btn_Color_BGLight.Enabled   := status;
    Btn_Color_BGDark.Enabled    := status;
    Btn_Color_Grid.Enabled      := status;
    Btn_Color_Num1.Enabled      := status;
    Btn_Color_Num2.Enabled      := status;
    Btn_Color_Num3.Enabled      := status;
    Btn_Color_Num4.Enabled      := status;
    Btn_Color_Num5.Enabled      := status;
    Btn_Color_Num6.Enabled      := status;
    Btn_Color_Num7.Enabled      := status;
    Btn_Color_Num8.Enabled      := status;
    Btn_Color_Num9.Enabled      := status;
    Btn_Color_Solved.Enabled    := status;
    Btn_Color_Correct.Enabled   := status;
    Btn_Color_Incorrect.Enabled := status;
  end;
begin
  EnableControls(CB_Themes.ItemIndex <> 0);
  settingsTemp.ItemIndex          := settingsTemp.mThemes.mDict.IndexOf(CB_Themes.Items[CB_Themes.ItemIndex]);
  Btn_Color_BGLight.ButtonColor   := settingsTemp.CurTheme.bg_light;
  Btn_Color_BGDark.ButtonColor    := settingsTemp.CurTheme.bg_dark;
  Btn_Color_Grid.ButtonColor      := settingsTemp.CurTheme.grid;
  Btn_Color_Num1.ButtonColor      := settingsTemp.CurTheme.numbers[1];
  Btn_Color_Num2.ButtonColor      := settingsTemp.CurTheme.numbers[2];
  Btn_Color_Num3.ButtonColor      := settingsTemp.CurTheme.numbers[3];
  Btn_Color_Num4.ButtonColor      := settingsTemp.CurTheme.numbers[4];
  Btn_Color_Num5.ButtonColor      := settingsTemp.CurTheme.numbers[5];
  Btn_Color_Num6.ButtonColor      := settingsTemp.CurTheme.numbers[6];
  Btn_Color_Num7.ButtonColor      := settingsTemp.CurTheme.numbers[7];
  Btn_Color_Num8.ButtonColor      := settingsTemp.CurTheme.numbers[8];
  Btn_Color_Num9.ButtonColor      := settingsTemp.CurTheme.numbers[9];
  Btn_Color_Solved.ButtonColor    := settingsTemp.CurTheme.solved;
  Btn_Color_Correct.ButtonColor   := settingsTemp.CurTheme.correct;
  Btn_Color_Incorrect.ButtonColor := settingsTemp.CurTheme.incorrect;
  //PB_Preview.Invalidate;
end;


procedure TFormOptions.Btn_AddClick(Sender : TObject);
var
  title  : String;
  isDone : Boolean;
begin
  title  := 'My best theme';
  isDone := False;
  while not(isDone) do
  begin
    if InputQuery('Create a new theme', 'New theme name', title) then
    begin
      if (settingsTemp.mThemes.mDict.IndexOf(title) = -1) then
      begin
        settingsTemp.mThemes.mDict.Add(title, DefaultTheme);
        CB_Themes.Items.Add(title);
        isEdited            := True;
        CB_Themes.ItemIndex := CB_Themes.Items.Count - 1;
        CB_ThemesSelect(self);

        isDone := True;
      end
      else
        isDone := (MessageDlg(
                    'Create a new theme',
                    'Error: name is taken. Please enter another one.',
                    mtError, mbOKCancel, 0) = mrCancel);
    end
    else isDone := True;
  end;
end;

procedure TFormOptions.Btn_DeleteClick(Sender : TObject);
var
  newIndex : Integer;
begin
  newIndex := CB_Themes.ItemIndex - 1;

  settingsTemp.mThemes.mDict.Remove(CB_Themes.Items[CB_Themes.ItemIndex]);
  CB_Themes.Items.Delete(CB_Themes.ItemIndex);

  settingsTemp.ItemIndex := newIndex;
  CB_Themes.ItemIndex    := settingsTemp.ItemIndex;
  CB_ThemesSelect(Self);

  isEdited := True;
end;

procedure TFormOptions.Btn_RenameClick(Sender : TObject);
var
  title  : String;
  isDone : Boolean;
begin
  title  := settingsTemp.mThemes.mDict.Keys[settingsTemp.ItemIndex];
  isDone := False;
  while not(isDone) do
  begin
    if InputQuery('Rename current theme',
                  'New theme name',
                  title) then
    begin
      if (settingsTemp.mThemes.mDict.IndexOf(title) = -1) then
      begin
        settingsTemp.mThemes.mDict.Keys[settingsTemp.ItemIndex] := title;
        CB_Themes.Items[CB_Themes.ItemIndex] := title;
        isEdited := True;
        isDone   := True;
      end
      else
        isDone := (MessageDlg(
                    'Create a new theme',
                    'Error: name is taken. Please enter another one.',
                    mtError, mbOKCancel, 0) = mrCancel);
    end
    else isDone := True;
  end;

end;

{ Color Buttons }
procedure TFormOptions.Btn_Color_Changed  (Sender : TObject);
begin
  themeTemp := settingsTemp.CurTheme;

  case TColorButton(Sender).Tag of
  TAG_BTNCOLOR_BGLIGHT:
  begin
    themeTemp.bg_light     := TColorButton(Sender).ButtonColor;
    preview_bBGLight.Color := themeTemp.bg_light;
  end;
  TAG_BTNCOLOR_BGDARK:
  begin
    themeTemp.bg_dark     := TColorButton(Sender).ButtonColor;
    preview_bBGDark.Color := themeTemp.bg_dark;
  end;
  TAG_BTNCOLOR_GRID:
  begin
    themeTemp.grid        := TColorButton(Sender).ButtonColor;
    preview_pen.Color     := themeTemp.grid;
  end;
  TAG_BTNCOLOR_NUM1:       themeTemp.numbers[1] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM2:       themeTemp.numbers[2] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM3:       themeTemp.numbers[3] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM4:       themeTemp.numbers[4] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM5:       themeTemp.numbers[5] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM6:       themeTemp.numbers[6] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM7:       themeTemp.numbers[7] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM8:       themeTemp.numbers[8] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_NUM9:       themeTemp.numbers[9] := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_CSOLVED:    themeTemp.solved     := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_CCORRECT:   themeTemp.correct    := TColorButton(Sender).ButtonColor;
  TAG_BTNCOLOR_CINCORRECT: themeTemp.incorrect  := TColorButton(Sender).ButtonColor;
  end;
  settingsTemp.CurTheme := themeTemp;
  isEdited              := True;
  PB_Preview.Invalidate;
end;

procedure TFormOptions.Btn_GenerateClick(Sender : TObject);
begin
  Btn_Color_BGLight.ButtonColor    := RandomColor;
  Btn_Color_Grid.ButtonColor       := RandomColor;
  Btn_Color_Num1.ButtonColor       := RandomColor;
  Btn_Color_Num2.ButtonColor       := RandomColor;
  Btn_Color_Num3.ButtonColor       := RandomColor;
  Btn_Color_Num4.ButtonColor       := RandomColor;
  Btn_Color_Num5.ButtonColor       := RandomColor;
  Btn_Color_Num6.ButtonColor       := RandomColor;
  Btn_Color_Num7.ButtonColor       := RandomColor;
  Btn_Color_Num8.ButtonColor       := RandomColor;
  Btn_Color_Num9.ButtonColor       := RandomColor;
  Btn_Color_Solved.ButtonColor     := RandomColor;
  Btn_Color_Correct.ButtonColor    := RandomColor;
  Btn_Color_Incorrect.ButtonColor  := RandomColor;
  PB_Preview.Invalidate;
end;


{ Settings save/not }

procedure TFormOptions.Btn_OkClick(Sender : TObject);
begin
  settingsTemp.mAutosave      := CheckBox_AutoSave.Checked;
  settingsTemp.mLoadOnStartup := CheckBox_StartupContinue.Checked;
  settingsTemp.ItemIndex      := settingsTemp.mThemes.mDict.IndexOf(CB_Themes.Items[CB_Themes.ItemIndex]);
  GameSettings.Copy(settingsTemp);
  GameSettings.Save;
  isEdited        := False;
  settingsChanged := True;
  Close;
end;

procedure TFormOptions.Btn_CancelClick(Sender : TObject);
begin
  settingsTemp.Copy(GameSettings);
  ResetControls;
  isEdited := False;
  Close;
end;

procedure TFormOptions.FormCloseQuery(Sender : TObject; var CanClose : Boolean);
begin
  CanClose := True;
  if isEdited then
  begin
     case MessageDlg('Game settings changed',
                'Save changes to game settings?',
                mtConfirmation,
                mbYesNoCancel,
                0) of
       mrYes:    Btn_OkClick(self);
       mrNo:     Btn_CancelClick(self);
       mrCancel: CanClose := False;
     end;
  end;
end;

end.
