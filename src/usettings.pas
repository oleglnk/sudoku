unit uSettings;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Graphics, fgl, inifiles;

const
  number_count = 9;
type
//
//  KTheme
//
KTheme = record
  bg_light  : TColor;
  bg_dark   : TColor;
  grid      : TColor;
  solved    : TColor;
  correct   : TColor;
  incorrect : TColor;
  numbers   : array[1..number_count] of TColor;
end;

//
//  KThemeDict
//
KThemeDict = specialize TFPGMap<String, KTheme>;

KThemes = class
const
  dFilename = 'themes.ini';
protected
  function  LoadTheme(aFile : TIniFile; aName : String) : KTheme;
  procedure SaveTheme(aFile : TIniFile; aName : String; aTheme : KTheme);
public
  mDict     : KThemeDict;
  mFilename : String;
  constructor Create;
  destructor  Destroy; override;
  procedure   Load;
  procedure   Save;
  procedure   Copy(const aSource : KThemes);
end;

//
//  KSettings
//
KSettings = class
const
  dSettingsSectionName = 'General';
  dFilename    = 'settings.ini';
public
  mFilename       : String;
  mThemes         : KThemes;
  mThemeIndex     : Integer;
  mAutosave       : Boolean;
  mLoadOnStartup  : Boolean;

  function  GetCurTheme : KTheme;
  procedure SetCurTheme(const aValue : KTheme);
public
  constructor Create;
  destructor  Destroy; override;
  procedure   Load;
  procedure   Save;
  procedure   Copy(const aSource : KSettings);

  property  ItemIndex : Integer read mThemeIndex  write mThemeIndex;
  property  CurTheme  : KTheme  read GetCurTheme  write SetCurTheme;
end;


var
  GameSettings : KSettings;

implementation

uses
  uUtils,
  uDataNConst;


{-------------------------------------------------------------------------------

                                  KThemeDict

-------------------------------------------------------------------------------}

function KThemes.LoadTheme(
    aFile : TIniFile;
    aName : String) : KTheme;
var
  i : integer;
begin
  Result.bg_light   := HexStrToTColor(
                    aFile.ReadString(aName, 'background light', ''),
                    DefaultTheme.bg_light);
  Result.bg_dark    := HexStrToTColor(
                    aFile.ReadString(aName, 'background dark', ''),
                    DefaultTheme.bg_dark);
  Result.grid       := HexStrToTColor(
                    aFile.ReadString(aName, 'grid', ''),
                    DefaultTheme.grid);
  Result.solved     := HexStrToTColor(
                    aFile.ReadString(aName, 'solved', ''),
                    DefaultTheme.solved);
  Result.correct    := HexStrToTColor(
                    aFile.ReadString(aName, 'correct', ''),
                    DefaultTheme.correct);
  Result.incorrect  := HexStrToTColor(
                    aFile.ReadString(aName, 'incorrect', ''),
                    DefaultTheme.incorrect);
  for i := 1 to number_count do
    Result.numbers[i] := HexStrToTColor(
                    aFile.ReadString(aName, 'num' + IntToStr(i), ''),
                    DefaultTheme.numbers[i]);
end;

procedure KThemes.SaveTheme(
    aFile  : TIniFile;
    aName  : String;
    aTheme : KTheme);
var
  i : integer;
begin
  aFile.WriteString(aName, 'background light', TColorToHexStr(aTheme.bg_light));
  aFile.WriteString(aName, 'background dark',  TColorToHexStr(aTheme.bg_dark));
  aFile.WriteString(aName, 'grid',             TColorToHexStr(aTheme.grid));
  aFile.WriteString(aName, 'solved',           TColorToHexStr(aTheme.solved));
  aFile.WriteString(aName, 'correct',          TColorToHexStr(aTheme.correct));
  aFile.WriteString(aName, 'incorrect',        TColorToHexStr(aTheme.incorrect));
  for i := 1 to number_count do
    aFile.WriteString(
      aName,
      'num' + IntToStr(i),
      TColorToHexStr(aTheme.numbers[i]));
end;

constructor KThemes.Create;
begin
  mFilename := dFilename;
  mDict     := KThemeDict.Create;
end;

destructor  KThemes.Destroy;
begin
  mDict.Free;
  inherited;
end;

procedure KThemes.Load;
var
  iniFile  : TIniFile;
  sections : TStringList;
  i        : integer;
begin

  mDict.clear;
  mDict.add(DefaultThemeTitle, DefaultTheme);

  sections := TStringList.Create;

  iniFile  := TIniFile.Create(mFilename);
  iniFile.ReadSections(sections);
  
  for i := 0 to sections.Count - 1 do
    if (AnsiCompareText(sections[i], DefaultThemeTitle) <> 0) then
    begin
      mDict.Add(sections[i], LoadTheme(iniFile, sections[i]));
    end;

  sections.Destroy;
  iniFile.Free;
end;

procedure KThemes.Save;
var
  iniFile : TIniFile;
  i : integer;
begin
  if FileExists(mFilename) then
    DeleteFile(mFilename);

  iniFile := TIniFile.Create(mFilename);

  for i := 0 to mDict.Count - 1 do
    if (mDict.keys[i] <> DefaultThemeTitle) then
      SaveTheme(iniFile, mDict.keys[i], mDict.Data[i]);

  iniFile.Free;
end;

procedure KThemes.Copy(
    const aSource : KThemes);
var
  i : integer;
begin
  mFilename := aSource.mFilename;
  mDict.Clear;
  for i := 0 to aSource.mDict.Count - 1 do
    mDict.Add(aSource.mDict.Keys[i], aSource.mDict.Data[i]);
end;


{-------------------------------------------------------------------------------

                                  KSettings

-------------------------------------------------------------------------------}

constructor KSettings.Create;
begin
  mThemes         := KThemes.Create;
  mFilename       := dFilename;
  mAutosave       := False;
  mLoadOnStartup  := False;
end;

destructor KSettings.Destroy;
begin
  mThemes.Destroy;
  inherited Destroy;
end;

procedure KSettings.Load;
var
  iniFile  : TIniFile;
  theme : String;
begin

  mThemes.Load;

  iniFile                  := TIniFile.Create(mFilename);
  iniFile.Options          := iniFile.Options + [ifoWriteStringBoolean];

  iniFile.BoolTrueStrings  := ['true'];
  iniFile.BoolFalseStrings := ['false'];

  mAutosave       := iniFile.ReadBool(dSettingsSectionName, 'autosave', False);
  mLoadOnStartup  := iniFile.ReadBool(dSettingsSectionName, 'load on startup', False);
  theme           := iniFile.ReadString(dSettingsSectionName, 'theme', '');

  mThemeIndex := mThemes.mDict.IndexOf(theme);
  if (mThemeIndex = -1) then
    mThemeIndex := mThemes.mDict.IndexOf(DefaultThemeTitle);
  iniFile.Free;
end;

procedure KSettings.Save;
var
  iniFile : TIniFile;
begin
  if FileExists(mFilename) then
    DeleteFile(mFilename);
  iniFile                  := TIniFile.Create(mFilename);
  iniFile.Options          := iniFile.Options + [ifoWriteStringBoolean];
  iniFile.BoolTrueStrings  := ['true'];
  iniFile.BoolFalseStrings := ['false'];
  
  iniFile.WriteBool  (dSettingsSectionName, 'autosave', mAutosave);
  iniFile.WriteBool  (dSettingsSectionName, 'load on startup', mLoadOnStartup);
  iniFile.WriteString(dSettingsSectionName, 'theme', mThemes.mDict.Keys[mThemeIndex]);

  iniFile.Free;

  mThemes.Save;
end;

procedure KSettings.Copy(
    const aSource : KSettings);
begin
  mAutosave       := aSource.mAutosave;
  mFilename       := aSource.mFilename;
  mLoadOnStartup  := aSource.mLoadOnStartup;
  mThemeIndex     := aSource.mThemeIndex;
  mThemes.Copy(aSource.mThemes);
end;

function  KSettings.GetCurTheme : KTheme;
begin
  Result := mThemes.mDict.Data[mThemeIndex];
end;

procedure KSettings.SetCurTheme(
    const aValue : KTheme);
begin
  mThemes.mDict.Data[mThemeIndex] := aValue;
end;

end.
