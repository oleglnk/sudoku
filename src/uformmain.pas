unit uFormMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ComCtrls, ExtCtrls, Types,
  uFormOptions,
  uFormStats,
  uFormAbout,
  uFormLoadSave,
  uSettings,
  uGameSudoku;

type

  { TFormMain }

  TFormMain = class(TForm)
    {            Main Menu           }
    MainMenu              : TMainMenu;
    MI_File               : TMenuItem;
    MI_File_New           : TMenuItem;
    MI_File_New_VeryEasy  : TMenuItem;
    MI_File_New_Easy      : TMenuItem;
    MI_File_New_Average   : TMenuItem;
    MI_File_New_Hard      : TMenuItem;
    MI_File_New_VeryHard  : TMenuItem;
    MI_File_Load          : TMenuItem;
    MI_File_Save          : TMenuItem;
    MI_File_Exit          : TMenuItem;
    MI_Game               : TMenuItem;
    MI_Game_SolveOne      : TMenuItem;
    MI_Game_CheckAll      : TMenuItem;
    MI_Game_SolveAll      : TMenuItem;
    MI_Options            : TMenuItem;
    MI_Options_Settings   : TMenuItem;
    MI_Options_Statistics : TMenuItem;
    MI_About              : TMenuItem;
    {           Pop-up Menu          }
    ContextMenu : TPopupMenu;
    MI_SetNum1    : TMenuItem;
    MI_SetNum2    : TMenuItem;
    MI_SetNum3    : TMenuItem;
    MI_SetNum4    : TMenuItem;
    MI_SetNum5    : TMenuItem;
    MI_SetNum6    : TMenuItem;
    MI_SetNum7    : TMenuItem;
    MI_SetNum8    : TMenuItem;
    MI_SetNum9    : TMenuItem;
    MI_Erase      : TMenuItem;
    {             Other              }
    Scene      : TPaintBox;
    GameStatus : TStatusBar;
    GameTimer  : TTimer;

    function  CanGameStop : Boolean;
    procedure GameOver(const aLose : Boolean = False);
    procedure ControlsEnable;
    procedure ControlsDisable;
    procedure ControlsSetTags;

    procedure GameTimerTimer(Sender : TObject);

    {   Paint Box Game Field    }

    procedure ScenePaint       (Sender : TObject);
    procedure SceneMouseMove   (Sender : TObject; Shift : TShiftState; X, Y : Integer);
    procedure SceneContextPopup(Sender : TObject; MousePos : TPoint; var Handled : Boolean);

    {   TFormMain    }

    procedure FormCreate    (Sender : TObject);
    procedure FormDestroy   (Sender : TObject);
    procedure FormCloseQuery(Sender : TObject; var CanClose : Boolean);
    procedure FormResize    (Sender : TObject);

    {   Pop Menu     }

    procedure PopUpMenuClick(Sender : TObject);

    {                }
    {   Main Menu    }
    {                }

    {   Menu File    }

    procedure MI_File_New_GameClick(Sender : TObject);
    procedure MI_File_LoadClick    (Sender : TObject);
    procedure MI_File_SaveClick    (Sender : TObject);
    procedure MI_File_ExitClick    (Sender : TObject);

    {   Menu Game    }

    procedure MI_Game_SolveOneClick(Sender : TObject);
    procedure MI_Game_CheckAllClick(Sender : TObject);
    procedure MI_Game_SolveAllClick(Sender : TObject);

    {   Menu Options }

    procedure MI_Options_SettingsClick  (Sender : TObject);
    procedure MI_Options_StatisticsClick(Sender : TObject);

    {   Menu About    }

    procedure MI_AboutClick(Sender : TObject);

  private
    isGameStarted : Boolean;
    selected_row : Integer;
    selected_col : Integer;
  public
  end;

var
  FormMain : TFormMain;

implementation

uses
  uUtils,
  uDataNConst;

const
  // Popup menu
  TAG_PMGAMEACTION_NUM0 = 0;  // Erase
  TAG_PMGAMEACTION_NUM1 = 1;
  TAG_PMGAMEACTION_NUM2 = 2;
  TAG_PMGAMEACTION_NUM3 = 3;
  TAG_PMGAMEACTION_NUM4 = 4;
  TAG_PMGAMEACTION_NUM5 = 5;
  TAG_PMGAMEACTION_NUM6 = 6;
  TAG_PMGAMEACTION_NUM7 = 7;
  TAG_PMGAMEACTION_NUM8 = 8;
  TAG_PMGAMEACTION_NUM9 = 9;
  // Main menu -> New Game
  TAG_MENU_FILE_NEW_VE  = Ord(kdVeryEasy);  // Very Easy
  TAG_MENU_FILE_NEW_E   = Ord(kdEasy);      // Easy
  TAG_MENU_FILE_NEW_A   = Ord(kdAverage);   // Average
  TAG_MENU_FILE_NEW_H   = Ord(kdHard);      // Hard
  TAG_MENU_FILE_NEW_VH  = Ord(kdVeryHard);  // Very Hard


{$R *.lfm}

function  TFormMain.CanGameStop : Boolean;
begin
  Result := True;
  if isGameStarted then
  begin
    Result := MessageDlg('Game in progress',
                'All progress will be lost.' + LineEnding +
                'Do you wand to stop the game?',
                mtConfirmation,
                mbYesNo,
                0) = mrYes;
  end;
end;

procedure TFormMain.GameOver(
    const aLose : Boolean = False);
const
  dCaption    = 'Game Over';
  dMsgFailure = 'Game Over';
  dMsgSuccess = 'CORRECT!';
var
  dlgMsg : String;
begin
  GameTimer.Enabled := False;
  if aLose then
    dlgMsg := dMsgFailure
  else
  begin
    dlgMsg := dMsgSuccess;
    with GameStats.mDifficulty[GameSudoku.mDifficulty] do
    begin
      played := played + 1;
      if best_time > GameSudoku.CurTime then
        best_time := GameSudoku.CurTime;
    end;
  end;

  MessageDlg(dCaption, dlgMsg, mtInformation, [mbOk], 0);
  GameSudoku.ClearAll;
  ControlsDisable;
end;

procedure TFormMain.ControlsEnable;
begin
  MI_Game_SolveOne.Caption := 
        'Solve One Cell (left: ' + 
        IntToStr(GameSudoku.HintSolveOne) +
        ')';
  MI_Game_CheckAll.Caption := 
        'Check All Cells (left: ' + 
        IntToStr(GameSudoku.HintCheckAll) +
        ')';
  isGameStarted             := True;
  MI_Game.Enabled           := True;
  MI_File_Save.Enabled      := True;
  MI_Game_SolveOne.Enabled  := (GameSudoku.HintSolveOne <> 0);
  MI_Game_CheckAll.Enabled  := (GameSudoku.HintCheckAll <> 0);
  GameTimer.Enabled         := True;
  GameStatus.Panels[2].Text := 'Hints left: '  + IntToStr(GameSudoku.HintCount);
  GameStatus.Panels[3].Text := 'Time passed: ' + SecondsToStr(GameSudoku.CurTime);
  Scene.Invalidate;
end;

procedure TFormMain.ControlsDisable;
begin
  isGameStarted             := False;
  MI_Game.Enabled           := False;
  GameTimer.Enabled         := False;
  MI_File_Save.Enabled      := False;
  GameStatus.Panels[2].Text := 'Hints left: 0';
  GameStatus.Panels[3].Text := 'Time passed: 00:00';
  Scene.Invalidate;
end;

procedure TFormMain.ControlsSetTags;
begin
  // Pop-up menu
  MI_Erase.Tag   := TAG_PMGAMEACTION_NUM0;
  MI_SetNum1.Tag := TAG_PMGAMEACTION_NUM1;
  MI_SetNum2.Tag := TAG_PMGAMEACTION_NUM2;
  MI_SetNum3.Tag := TAG_PMGAMEACTION_NUM3;
  MI_SetNum4.Tag := TAG_PMGAMEACTION_NUM4;
  MI_SetNum5.Tag := TAG_PMGAMEACTION_NUM5;
  MI_SetNum6.Tag := TAG_PMGAMEACTION_NUM6;
  MI_SetNum7.Tag := TAG_PMGAMEACTION_NUM7;
  MI_SetNum8.Tag := TAG_PMGAMEACTION_NUM8;
  MI_SetNum9.Tag := TAG_PMGAMEACTION_NUM9;

  // Main menu -> New Game
  MI_File_New_VeryEasy.Tag := TAG_MENU_FILE_NEW_VE;
  MI_File_New_Easy.Tag     := TAG_MENU_FILE_NEW_E;
  MI_File_New_Average.Tag  := TAG_MENU_FILE_NEW_A;
  MI_File_New_Hard.Tag     := TAG_MENU_FILE_NEW_H;
  MI_File_New_VeryHard.Tag := TAG_MENU_FILE_NEW_VH;
end;

procedure TFormMain.GameTimerTimer(Sender : TObject);
begin
  GameSudoku.CurTime       := GameSudoku.CurTime + 1;
  GameStatus.Panels[3].Text := 'Time passed: ' + SecondsToStr(GameSudoku.CurTime);
end;

{   Paint Box Game Field    }

procedure TFormMain.SceneMouseMove(
    Sender  : TObject;
    Shift   : TShiftState;
    X, Y    : Integer);
begin
  GameStatus.Panels[0].Text := 'X: ' + IntToStr(X);
  GameStatus.Panels[1].Text := 'Y: ' + IntToStr(Y);
end;

procedure TFormMain.ScenePaint(Sender : TObject);
begin
  GameSudoku.mField.Draw(Scene.Canvas);
end;

procedure TFormMain.SceneContextPopup(
    Sender      : TObject;
    MousePos    : TPoint;
    var Handled : Boolean);
begin
  if isGameStarted then
  begin
    GameSudoku.mField.RowColByPos(MousePos.X, MousePos.Y, selected_row, selected_col);
    Handled := GameSudoku.mField.Defaults[selected_row, selected_col];
  end
  else
    Handled := True;
end;


{ TFormMain }

procedure TFormMain.FormCreate(Sender : TObject);
begin
  Randomize;

  ControlsSetTags;

  GameSettings := KSettings.Create;
  GameStats    := KStatistics.Create;
  GameSudoku   := GGameState.Create;

  GameSettings.Load;
  GameStats.Load;
  GameSudoku.mField.Resize(Scene.Width, Scene.Height);
  GameSudoku.mField.SetTheme(GameSettings.CurTheme);

  if (GameSettings.mLoadOnStartup) then
  begin
    if GameSudoku.Load(DefaultSavesDir + 'autosave' + DefaultSavesExt) then
      ControlsEnable;
  end
  else
    ControlsDisable;
end;

procedure TFormMain.FormDestroy(Sender : TObject);
begin
  GameSudoku.Free;
  GameStats.Free;
  GameSettings.Free;
end;

procedure TFormMain.FormCloseQuery(Sender : TObject; var CanClose : Boolean);
begin
  CanClose := True;
  GameStats.Save;
  if isGameStarted and GameSettings.mAutosave then
    GameSudoku.Save(DefaultSavesDir + 'autosave' + DefaultSavesExt)
  else
    CanClose := CanGameStop;
end;

procedure TFormMain.FormResize(Sender : TObject);
begin
  GameSudoku.mField.Resize(Scene.Width, Scene.Height);
  Scene.Invalidate;
end;


{                }
{   Pop Menu     }
{                }

procedure TFormMain.PopUpMenuClick(Sender : TObject);
var
  number : Int64;
begin
  number := TMenuItem(Sender).Tag;
  GameSudoku.mField.Values[selected_row, selected_col] := number;
  Scene.Invalidate;
  if (number <> 0) then
    if GameSudoku.IsOver then
      GameOver;
end;


{                }
{   Main Menu    }
{                }

{   Menu File    }

procedure TFormMain.MI_File_New_GameClick(Sender : TObject);
begin
  if CanGameStop then
  begin
    GameSudoku.generate(KDifficulty(TMenuItem(Sender).Tag));
    ControlsEnable;
  end;
end;

procedure TFormMain.MI_File_LoadClick(Sender : TObject);
begin
  if CanGameStop then
  begin
    ControlsDisable;
    GameSudoku.ClearAll;
    FormLoadSave.mKind := kfkLoad;
    FormLoadSave.ShowModal;
    if FormLoadSave.IsGameLoad then
    begin
      ControlsEnable;
    end;
  end;
end;

procedure TFormMain.MI_File_SaveClick(Sender : TObject);
begin
  GameTimer.Enabled  := False;
  FormLoadSave.mKind := kfkSave;
  FormLoadSave.ShowModal;
  GameTimer.Enabled  := True;
end;

procedure TFormMain.MI_File_ExitClick(Sender : TObject);
begin
  Close;
end;

{   Menu Game    }

procedure TFormMain.MI_Game_SolveOneClick(Sender : TObject);
begin
  GameSudoku.SolveOne;
  MI_Game_SolveOne.Caption := 'Solve One Cell (left: ' + IntToStr(GameSudoku.HintSolveOne) + ')';
  GameStatus.Panels[2].Text := 'Hints left: ' + IntToStr(GameSudoku.HintCount);
  if (GameSudoku.HintSolveOne = 0) then
  begin
    MI_Game_SolveOne.Enabled := False;
  end;
  if GameSudoku.IsOver then
    GameOver;
  Scene.Invalidate;
end;

procedure TFormMain.MI_Game_CheckAllClick(Sender : TObject);
begin
  GameSudoku.CheckAll;
  MI_Game_CheckAll.Caption := 'Check All Cells (left: ' + IntToStr(GameSudoku.HintCheckAll) + ')';
  GameStatus.Panels[2].Text := 'Hints left: ' + IntToStr(GameSudoku.HintCount);
  if (GameSudoku.HintCheckAll = 0) then
  begin
    MI_Game_CheckAll.Enabled := False;
  end;
  Scene.Invalidate;
end;

procedure TFormMain.MI_Game_SolveAllClick(Sender : TObject);
begin
  GameSudoku.Solve;
  Scene.Invalidate;
  GameOver(True);
end;


{   Menu Options }

procedure TFormMain.MI_Options_SettingsClick(Sender : TObject);
begin
  FormOptions.settingsChanged := False;
  FormOptions.ShowModal;
  if FormOptions.settingsChanged then
  begin
    GameSudoku.mField.SetTheme(GameSettings.CurTheme);
    Scene.Invalidate;
  end;
end;

procedure TFormMain.MI_Options_StatisticsClick(Sender : TObject);
begin
  FormStats.ShowStats;
  FormStats.ShowModal;
end;


{   Menu About    }

procedure TFormMain.MI_AboutClick(Sender : TObject);
begin
  FormAbout.ShowModal;
end;

end.
