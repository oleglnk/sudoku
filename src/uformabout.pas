unit uFormAbout;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  uControls;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    LabelDescription : TLabel;
    LabelLicense : TLabel;
    LabelLicenseGame : TLabel;          //Different Labels for different hints
    LabelLicenseAnd : TLabel;
    LabelLicenseLazarusFP : TLabel;
    LabelSourceCode : TLabel;
    LabelLazarus : TLabel;
    LabelFP : TLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    LinkFreePascal : KLink;
    LinkLazarus    : KLink;
    LinkSourceCode : KLink;
  public

  end;

var
  FormAbout : TFormAbout;

implementation

{$R *.lfm}


procedure TFormAbout.FormCreate(Sender: TObject);
begin
  LinkSourceCode := KLink.Create(Self);
  with LinkSourceCode do
  begin
    Caption := 'https://gitlab.com/oleglnk';
    URL     := 'https://gitlab.com/oleglnk/sudoku';

    AnchorSide[akTop].Control   := LabelSourceCode;
    AnchorSide[akTop].Side      := asrCenter;
    AnchorSide[akLeft].Control  := LabelLicenseGame;
    AnchorSide[akLeft].Side     := asrLeft;
  end;
  LinkLazarus := KLink.Create(Self, 'https://www.lazarus-ide.org');
  with LinkLazarus do
  begin
    AnchorSide[akTop].Control   := LabelLazarus;
    AnchorSide[akTop].Side      := asrCenter;
    AnchorSide[akLeft].Control  := LinkSourceCode;
    AnchorSide[akLeft].Side     := asrLeft;
  end;
  
  LinkFreePascal := KLink.Create(Self, 'https://www.freepascal.org');
  with LinkFreePascal do
  begin
    AnchorSide[akTop].Control   := LabelFP;
    AnchorSide[akTop].Side      := asrCenter;
    AnchorSide[akLeft].Control  := LinkLazarus;
    AnchorSide[akLeft].Side     := asrLeft;
  end;
end;

procedure TFormAbout.FormDestroy(Sender: TObject);
begin
  LinkFreePascal.Free;
  LinkLazarus.Free;
  LinkSourceCode.Free;
end;

end.
