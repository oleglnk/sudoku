unit uControls;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Controls, StdCtrls, ExtCtrls, Graphics;

type

//
//  KView
//

KView = class(TPanel)
protected

public
  constructor Create(TheOwner: TComponent); override;
  destructor Destroy; override;
end;

//
//  KButton
//

KButtonStyle = (kbsBasic, kbsHover);

KButton = class(TLabel)
private
  EButtonStyle : KButtonStyle;

  procedure DrawBorderBox;
  procedure DrawShadowBox;

  procedure SetButtonStyle(const AValue : KButtonStyle);
public
  constructor Create(TheOwner: TComponent); override;
  destructor Destroy; override;

  procedure Paint; override;

  procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  procedure MouseEnter; override;
  procedure MouseLeave; override;
end;

//
//  KLink
//
KLinkStyle = (klsBasic, klsHover);

KLink = class(TLabel)
private
  EUrl : String;
  procedure SetLinkStyle(const AValue : KLinkStyle);
  procedure SetUrl(const AValue : String);
public
  constructor Create(TheOwner: TComponent); override; overload;
  constructor Create(TheOwner: TComponent; const AUrl : String); overload;
  destructor Destroy; override;

  procedure MouseEnter; override;
  procedure MouseLeave; override;
  procedure Click; override;

  property URL : String read EUrl write SetUrl;
end;

//
//  KScene
//

KScene = class(TPaintBox)
protected

public
  constructor Create(TheOwner: TComponent); override;
  destructor Destroy; override;
  //procedure Paint; override;
end;


//================ IMPLEMENTATION =================//

implementation

uses
  lclintf;

//
//  KView
//

constructor KView.Create(TheOwner: TComponent);
begin
  inherited Create (TheOwner);
  Parent      := (TheOwner as TWinControl);
  Align       := alClient;
  BevelOuter  := bvNone;
end;

destructor KView.Destroy;
begin
  inherited;
end;



//
//  KButton
//

constructor KButton.Create(TheOwner: TComponent);
begin
  inherited Create (TheOwner);
  Parent := (TheOwner as TWinControl);
  Caption           := 'Button Name';
  Alignment         := taCenter;
  Layout            := tlCenter;
  AutoSize          := False;
  Width             := 120;
  Height            := 30;
  Self.Transparent  := False;

  SetButtonStyle(kbsBasic);
end;

destructor KButton.Destroy;
begin
  inherited;
end;

procedure KButton.SetButtonStyle(const AValue : KButtonStyle);
begin
  EButtonStyle := AValue;
  case EButtonStyle of
    kbsBasic:
      begin
        Color      := clWhite;
        Font.Color := clBlack;
        Font.Style := [];
      end;
    kbsHover:
      begin
        Color      := clAqua;
        Font.Color := clBlue;
        Font.Style := [fsBold];
      end;
  end;
end;

procedure KButton.DrawBorderBox;
begin
  case EButtonStyle of
    kbsBasic:
      begin
        Canvas.Pen.Color := clBlack;
        Canvas.Rectangle(0, 0, Width, Height);
      end;
    kbsHover:
      begin
        Canvas.Pen.Color := clWhite;
        Canvas.Rectangle(1, 1, Width - 1, Height - 1);
      end;
  end;
  
end;

procedure KButton.DrawShadowBox;
begin
  //
end;

procedure KButton.Paint;
begin
  inherited;
  //DrawShadowBox;
  DrawBorderBox;
end;

procedure KButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Invalidate;
end;

procedure KButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Invalidate;
end;

procedure KButton.MouseEnter;
begin
  SetButtonStyle(kbsHover);
  inherited;
end;

procedure KButton.MouseLeave;
begin
  SetButtonStyle(kbsBasic);
  inherited;
end;



//
//  KLink
//

constructor KLink.Create(TheOwner: TComponent);
begin
  Create(TheOwner, 'https://www.example.com/');
end;

constructor KLink.Create(TheOwner: TComponent; const AUrl : String);
begin
  inherited Create (TheOwner);
  Parent   := (TheOwner as TWinControl);
  ShowHint := True;
  Caption  := AUrl;
  URL      := AUrl;

  Cursor     := crHandPoint;
  Font.Style := [fsBold, fsUnderline];

  SetLinkStyle(klsBasic);
end;

destructor KLink.Destroy;
begin
  inherited;
end;


procedure KLink.SetUrl(const AValue : String);
begin
  EUrl := AValue;
  Hint := 'Open: ' + EUrl;
end;

procedure KLink.SetLinkStyle(const AValue : KLinkStyle);
begin
  case AValue of
    klsBasic:
      begin
        Font.Color := clBlue;
      end;
    klsHover:
      begin
        Font.Color := clRed;
      end;
  end;
end;

procedure KLink.MouseEnter;
begin
  SetLinkStyle(klsHover);
  inherited;
end;

procedure KLink.MouseLeave;
begin
  SetLinkStyle(klsBasic);
  inherited;
end;

procedure KLink.Click;
begin
  OpenURL(EUrl);
  inherited;
end;



//
//  KScene
//

constructor KScene.Create(TheOwner: TComponent);
begin
  inherited Create (TheOwner);
{  Parent      := (TheOwner as TWinControl)};
end;

destructor KScene.Destroy;
begin
  inherited;
end;

end.

