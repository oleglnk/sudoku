unit uFormStats;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  uGameSudoku;

type

  { TFormStats }

  TFormStats = class(TForm)
    BtnClear : TButton;
    GroupVeryEasy : TGroupBox;
    GroupEasy : TGroupBox;
    GroupAverage : TGroupBox;
    GroupHard : TGroupBox;
    GroupVeryHard : TGroupBox;
    Label_Hints_Caption1 : TLabel;
    Label_Hints_Caption2 : TLabel;
    Label_Hints_Caption3 : TLabel;
    Label_Hints_Caption4 : TLabel;
    Label_Hints_Value1 : TLabel;
    Label_Hints_Value2 : TLabel;
    Label_Hints_Value3 : TLabel;
    Label_Hints_Value4 : TLabel;
    Label_Played_Caption : TLabel;
    Label_Hints_Caption : TLabel;
    Label_Played_Caption1 : TLabel;
    Label_Played_Caption2 : TLabel;
    Label_Played_Caption3 : TLabel;
    Label_Played_Caption4 : TLabel;
    Label_Played_Value1 : TLabel;
    Label_Played_Value2 : TLabel;
    Label_Played_Value3 : TLabel;
    Label_Played_Value4 : TLabel;
    Label_Time_Caption : TLabel;
    Label_Played_Value : TLabel;
    Label_Hints_Value : TLabel;
    Label_Time_Caption1 : TLabel;
    Label_Time_Caption2 : TLabel;
    Label_Time_Caption3 : TLabel;
    Label_Time_Caption4 : TLabel;
    Label_Time_Value : TLabel;
    Label_Time_Value1 : TLabel;
    Label_Time_Value2 : TLabel;
    Label_Time_Value3 : TLabel;
    Label_Time_Value4 : TLabel;
    procedure BtnClearClick(Sender : TObject);
  private

  public
    procedure ShowStats;
  end;

var
  FormStats : TFormStats;

implementation

uses
  uUtils;

{$R *.lfm}

{ TFormStats }

procedure TFormStats.ShowStats;
begin
  Label_Hints_Value.Caption   := IntToStr(GameStats.mDifficulty[kdVeryHard].hints_used);
  Label_Hints_Value1.Caption  := IntToStr(GameStats.mDifficulty[kdEasy].hints_used);
  Label_Hints_Value2.Caption  := IntToStr(GameStats.mDifficulty[kdAverage].hints_used);
  Label_Hints_Value3.Caption  := IntToStr(GameStats.mDifficulty[kdHard].hints_used);
  Label_Hints_Value4.Caption  := IntToStr(GameStats.mDifficulty[kdVeryHard].hints_used);

  Label_Played_Value.Caption  := IntToStr(GameStats.mDifficulty[kdVeryHard].played);
  Label_Played_Value1.Caption := IntToStr(GameStats.mDifficulty[kdEasy].played);
  Label_Played_Value2.Caption := IntToStr(GameStats.mDifficulty[kdAverage].played);
  Label_Played_Value3.Caption := IntToStr(GameStats.mDifficulty[kdHard].played);
  Label_Played_Value4.Caption := IntToStr(GameStats.mDifficulty[kdVeryHard].played);

  Label_Time_Value.Caption    := SecondsToStr(GameStats.mDifficulty[kdVeryHard].best_time);
  Label_Time_Value1.Caption   := SecondsToStr(GameStats.mDifficulty[kdEasy].best_time);
  Label_Time_Value2.Caption   := SecondsToStr(GameStats.mDifficulty[kdAverage].best_time);
  Label_Time_Value3.Caption   := SecondsToStr(GameStats.mDifficulty[kdHard].best_time);
  Label_Time_Value4.Caption   := SecondsToStr(GameStats.mDifficulty[kdVeryHard].best_time);
end;

procedure TFormStats.BtnClearClick(Sender : TObject);
begin
  GameStats.Clear;
  ShowStats;
end;

end.
