unit uDataNConst;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Graphics,
  uSettings,
  uGameSudoku;


const
  DEFAULT_NUMBER_COUNT = 9;

  DefaultThemeTitle = 'default';
  DefaultTheme : KTheme = 
  (
    bg_light  : clWhite;
    bg_dark   : clGray;
    grid      : clBlack;
    solved    : clBlue;
    correct   : clGreen;
    incorrect : clRed;
    numbers   : (clBlack, clBlack, clBlack, clBlack, clBlack, clBlack, clBlack, clBlack, clBlack)
  );


  KDifficultyTitles : array[KDifficulty] of String = 
  (
    'Very Easy',
    'Easy',
    'Average',
    'Hard',
    'Very Hard'
  );

  KDifficultyMaxHints : array[KDifficulty, 0..1] of Integer =
  ( 
    (3, 3),
    (3, 2),
    (2, 2),
    (2, 1),
    (1, 1)
  );

  DefaultSavesDir = 'SavedGames/';
  DefaultSavesExt = '.ssg';  

implementation

end.
